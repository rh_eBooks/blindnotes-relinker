import sys, os, zipfile, shutil, re, tempfile, time, logging
import xml.dom.minidom as minidom
from epub_file import EpubFile
from epub import Epub
from epub_reader import EpubReader

correct_folder = os.path.dirname(sys.argv[1])
epub_file_name = os.path.basename(sys.argv[1])
epub_name = os.path.splitext(epub_file_name)[0]

logging.basicConfig(filename=correct_folder + '/' + epub_name + '_error-log.txt', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

def make_backup_file(epub):
    zip_in = zipfile.ZipFile(epub, 'a')
    backup_file_name = os.path.dirname(epub) + "/" + epub_name + "_backup.epub"
    file_out = open(backup_file_name, 'w')
    zip_out = zipfile.ZipFile(file_out, 'w')
    now = time.localtime(time.time())[:6]
    for zipInfo in zip_in.infolist():
        orig_filename = zipInfo.orig_filename
        data = zip_in.read(orig_filename)
        newInfo = zipfile.ZipInfo(orig_filename)
        newInfo.date_time = now
        if orig_filename != 'mimetype':
            newInfo.compress_type = zipfile.ZIP_DEFLATED    
        zip_out.writestr(newInfo, data)
    zip_out.close()
    zip_in.close()
    file_out.close()

#Return a list of all files in the EPUB's OEBPS.
def get_oebps_files(epub):
    epub_reader = EpubReader()
    list_o_files = epub_reader.get_from_file(epub)
    return list_o_files

#Return the file name, without the path, of a file from the OEBPS.    
def get_filename(file):
    xhtml_file_name = ((file.get_path()).split('OEBPS/'))[1]
    return xhtml_file_name

#Return the file name of the notes section.    
def get_notes_filename(file_list):
    for file in file_list.files:
        if file._is_html() == True:
            if (file.get_path().lower()).find('nts') != -1:
                notes_file_name = get_filename(file)
                return notes_file_name
                
#Remove AeP-produced error messages from Notes file.
def remove_error_messages(dom):
    p_tags = dom.getElementsByTagName('p')
    for p_tag in p_tags:
        if p_tag.hasAttribute('class') and (p_tag.getAttribute('class').find('para-en ') != -1) and (((p_tag.firstChild).firstChild.nodeValue).find('[[*** no matching reference') != -1):
            p_tag.removeChild(p_tag.firstChild)
            if p_tag.firstChild.tagName == "error":
                p_tag.removeChild(p_tag.firstChild)
            else:
                continue
    return dom

#Collect page anchors from XHTML pages in EPUB.
def create_page_anchor_dict(file_list):
    master_endnote_anchor_list = []
    for file in file_list.files:
        endnote_anchor_dict = {}
        if file._is_html() == True:
            xhtml_file_name = get_filename(file)
            dom = minidom.parseString(file.get_data())
            span_tags = dom.getElementsByTagName('span')
            for span_tag in span_tags:
                if span_tag.hasAttribute('id'):
                    if (span_tag.getAttribute('id')).find('Y_d-EndnotePhraseInText') != -1:
                        endnote_number = re.findall('\d{1,3}', span_tag.getAttribute('id'))
                        endnote_anchor_dict[endnote_number[0]] = xhtml_file_name + "#" + span_tag.getAttribute('id')
        if len(endnote_anchor_dict) > 0:
            master_endnote_anchor_list.append(endnote_anchor_dict)
    return master_endnote_anchor_list
    
def get_no_quotes_keyphrase(keyphrase):
    if not re.match('\w', keyphrase[0]):
        cleaned_keyphrase = re.search('\w+((.)*(\s\w+(.)*)+\w)*', keyphrase)
        try:
            return cleaned_keyphrase.group(0)
        except:
            return keyphrase
    else:
        return keyphrase

def create_blindnote_anchor(number):
    anchor = '<span id="Y_d-EndnotePhraseInText' + number + '"/>'
    return anchor

def insert_blindnote_anchor(p_tag, child_node, unlinked_reference, cleaned_keyphrase, dom):
    
    print unlinked_reference[0]
    new_node = dom.createElement('span')
    new_node.setAttribute('id', 'Y_d-EndnotePhraseInText' + unlinked_reference[0])
    index_of_phrase = (child_node.nodeValue).find(cleaned_keyphrase)
    bullshit = True
    initial_text = child_node.nodeValue
    if p_tag.nodeName == 'p':
        #print "version 1"
        first_node_text = initial_text[:index_of_phrase]
        second_node_text = initial_text[index_of_phrase:]
        first_node = dom.createTextNode(first_node_text)
        second_node = dom.createTextNode(second_node_text)
    #if p_tag.nodeName == 'p':
        try:
            p_tag.insertBefore(first_node, child_node)
            p_tag.insertBefore(new_node, child_node)
            p_tag.replaceChild(second_node, child_node)
        except: 
            bullshit = False
    if p_tag.nodeName == 'span':
        #try:
        #print "version 2"
        p_tag.insertBefore(new_node, child_node)
        '''except:
            bullshit = False'''
    return p_tag, child_node, dom, bullshit

def write_new_files(rewritten_doms):
    for rewritten_file in rewritten_doms:
        rewritten_file_list = list(rewritten_file)
        dom_full = (rewritten_file_list[1].toxml()).encode('utf8')
        with open(rewritten_file_list[0], 'w') as f:
            f.write(dom_full)

#If fix_hrefs_in_notes_section couldn't find a matching reference in the EPUB due to Easypress paft-to-sp string cleaning, find the phrase, insert missing anchor, relink.
def find_failed_links_and_relink(unlinked_references, file_list, notes_dom):
    rewritten_doms = []
    for unlinked_reference in unlinked_references:
        cleaned_keyphrase = get_no_quotes_keyphrase(unlinked_reference[1].firstChild.nodeValue)
        unlinked_reference[1] = cleaned_keyphrase
    for file in file_list.files:
        if file._is_html() == True and ((file.get_path().lower()).find('nts') == -1):
            xhtml_file_name = get_filename(file)
            dom = minidom.parseString(file.get_data())
            p_tags = dom.getElementsByTagName('p')
            for p_tag in p_tags:
                if p_tag.hasAttribute('class'):
                    #RHML tags that are contained inside blockquote transformations from Easypress.
                    if ((p_tag.getAttribute('class')).find('para-sp') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-pf') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-ul') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-cep') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-apext') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-bmep') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-bmext') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-ep') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-ext') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-fmep') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-gdans') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-gdqu') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-lt') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-pep') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-pq') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-tep') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-ans') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-ladd') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-lda') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-lps') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-lsa') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-ntext') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-ntv') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-pq') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-qu') != -1) or \
                        ((p_tag.getAttribute('class')).find('para-wl') != -1) or \
                        (((p_tag.getAttribute('class')).find('para-bx') != -1) and ((p_tag.getAttribute('class')).find('para-bxaft') == -1) and ((p_tag.getAttribute('class')).find('para-bxul') == -1)) or \
                        (((p_tag.getAttribute('class')).find('para-sb') != -1) and ((p_tag.getAttribute('class')).find('para-sbbl') == -1) and ((p_tag.getAttribute('class')).find('para-sbnl') == -1)) or \
                        ((p_tag.getAttribute('class')).find('list0') != -1) or \
                        ((p_tag.getAttribute('class')).find('list1') != -1) or \
                        ((p_tag.getAttribute('class')).find('list2') != -1) or \
                        ((p_tag.getAttribute('class')).find('footnote') != -1) or \
                        ((p_tag.getAttribute('class')).find('figcaption_para') != -1) or \
                        ((p_tag.getAttribute('class')).find('figcopyright') != -1) or \
                        ((p_tag.getAttribute('class')).find('primary') != -1) or \
                        ((p_tag.getAttribute('class')).find('secondary') != -1) or \
                        ((p_tag.getAttribute('class')).find('tertiary') != -1) or \
                        ((p_tag.getAttribute('class')).find('quaternary') != -1):
                        for unlinked_reference in unlinked_references:
                            child_nodes = p_tag.childNodes
                            for child_node in child_nodes:
                                #print child_node.nodeName
                                if child_node.nodeName == "span":
                                    span_children = child_node.childNodes
                                    for span_child in span_children:
                                        if span_child.nodeType == 3:
                                            if (span_child.nodeValue).find(unlinked_reference[1]) != -1:
                                                while len(unlinked_reference) == 3:
                                                    child_node, span_child, dom, bullshit = insert_blindnote_anchor(child_node, span_child, unlinked_reference, unlinked_reference[1], dom)
                                                    i_am_here = xhtml_file_name
                                                    unlinked_reference.extend((i_am_here, bullshit))
                                                    #print unlinked_reference
                                                #print unlinked_reference
                                            #print unlinked_reference    #return
                                             #print unlinked_reference
                                if child_node.nodeType == 3:
                                    if (child_node.nodeValue).find(unlinked_reference[1]) != -1:
                                        while len(unlinked_reference) == 3:
                                            p_tag, child_node, dom, bullshit = insert_blindnote_anchor(p_tag, child_node, unlinked_reference, unlinked_reference[1], dom)
                                            i_am_here = xhtml_file_name
                                            unlinked_reference.extend((i_am_here, bullshit))
                        rewritten_file_info = (xhtml_file_name, dom)
                        rewritten_doms.append(rewritten_file_info)
    #print unlinked_references
    '''for unlinked_reference in unlinked_references:
        print unlinked_reference[1]'''
    #print unlinked_references
    for unlinked_reference in unlinked_references:
        if len(unlinked_reference) == 3:
            unlinked_reference.append("LOOK FOR KEYPHRASE IN OR BETWEEN THESE FILES: " + unlinked_references[unlinked_references.index(unlinked_reference) - 1][3] + " to " + unlinked_references[unlinked_references.index(unlinked_reference) + 1][3])
            unlinked_reference.append(False)
            logging.debug("A blindnote text anchor failed to generate. Copy and paste the following anchor alongside the corresponding text in the following XHTML document: \n KEY PHRASE: " + unlinked_reference[1] + " \n ANCHOR TO COPY AND PASTE: <span id='Y_d-EndnotePhraseInText" + str(unlinked_reference[0]) + "/> \n LOCATION OF KEY PHRASE: " + unlinked_reference[3])
        #print unlinked_reference                                   
    for item1, item2 in zip(rewritten_doms,rewritten_doms[1:]):
        if item1[0] == item2[0]:
            if len(item1[1].toxml()) > len(item2[1].toxml()):
                rewritten_doms.pop(rewritten_doms.index(item1))
            else:
                rewritten_doms.pop(rewritten_doms.index(item2))
        else:
            continue
    #write_new_files(rewritten_doms)                              
    a_tags = notes_dom.getElementsByTagName('a')
                            
    
    for a_tag in a_tags:
        if a_tag.hasAttribute('href'):
            for unlinked_reference in unlinked_references:
                if (a_tag.getAttribute('href') == ('#Y_EndnotePhraseInText' + unlinked_reference[0])) and ((a_tag.getAttribute('href')).find('.xhtm') == -1):
                    #print unlinked_reference
                    if unlinked_reference[4] == False:
                        continue
                        #logging.debug("A blindnote text anchor failed to generate. Copy and paste the following anchor alongside the corresponding text in the following XHTML document: \n KEY PHRASE: " + unlinked_reference[1] + " \n ANCHOR TO COPY AND PASTE: <span id='Y_d-EndnotePhraseInText" + str(unlinked_reference[0]) + "/> \n LOCATION OF KEY PHRASE: " + unlinked_reference[3])
                    else:
                        #print "hey"
                        a_tag.setAttribute('href',unlinked_reference[3] + '#Y_d-EndnotePhraseInText' + unlinked_reference[0]) 
                    
    #print "yo"
    return notes_dom, rewritten_doms                  

#Get list of endnotes anchors we need to relink. Sigh.
def fix_hrefs_in_notes_section(file_list, notes_filename, master_endnote_anchor_list):
    rewritten_doms = []
    unlinked_references = []
    for file in file_list.files:
        if (file.get_path().lower()).find(notes_filename) != -1:
            dom = minidom.parseString(file.get_data())
            a_tags = dom.getElementsByTagName('a')
            a_tags_reference_link = []
            for a_tag in a_tags:
                if a_tag.hasAttribute('href'):
                    if (a_tag.getAttribute('href')).find('#Y_EndnotePhraseInText') != -1:
                        endnote_number = ((re.findall('\d{1,3}', a_tag.getAttribute('href')))[0], a_tag, False)
                        a_tags_reference_link.append(list(endnote_number))
            for a_tag_reference in a_tags_reference_link:
                for xhtml_page in master_endnote_anchor_list:
                    for key in xhtml_page:
                        if key == a_tag_reference[0]:
                            a_tag_reference[2] = True
                            a_tag_reference[1].setAttribute('href', xhtml_page.get(key))
                        else:
                            continue
                if a_tag_reference[2] == False:
                    unlinked_references.append(a_tag_reference)
            if len(unlinked_references) > 0:
                notes_dom, rewritten_doms = find_failed_links_and_relink(unlinked_references, file_list, dom)
                return notes_dom, rewritten_doms
            else:
                return dom, rewritten_doms

def write_new_notes_section(dom, file_name):
    just_notes_dom = list(dom)[0]
    just_rewritten_doms = list(dom)[1]
    dom_cleaned = remove_error_messages(just_notes_dom)
    dom_full = (dom_cleaned.toxml()).encode('utf8')
    return just_rewritten_doms, dom_full
    '''with open(file_name, "w") as f:
        f.write(dom_full)
        return f, just_rewritten_doms, dom_full'''
        
#        
def create_epub_with_new_notes(epub, new_notes_dom, rewritten_doms):
    make_backup_file(epub)
    zip_in = zipfile.ZipFile(epub, 'a')
    temp_file_name = os.path.abspath(epub) + ".new"
    file_out = open(temp_file_name, 'w')
    zip_out = zipfile.ZipFile(file_out, 'w')
    now = time.localtime(time.time())[:6]
    for zipInfo in zip_in.infolist():
        orig_filename = zipInfo.orig_filename
        data = zip_in.read(orig_filename)
        newInfo = zipfile.ZipInfo(orig_filename)
        newInfo.date_time = now
        if orig_filename != 'mimetype':
            newInfo.compress_type = zipfile.ZIP_DEFLATED
        if len(rewritten_doms) > 0:
            for rewritten_dom in rewritten_doms:
                if orig_filename.find(rewritten_dom[0]) != -1:
                    zip_out.writestr(newInfo, (rewritten_dom[1].toxml()).encode('utf8'))
        if orig_filename.find('_nts') != -1:
            zip_out.writestr(newInfo, new_notes_dom)
        else:
            if orig_filename in zip_out.NameToInfo:
                continue
            else:
                zip_out.writestr(newInfo, data)
    zip_out.close()
    zip_in.close()
    file_out.close()
    os.rename(temp_file_name, os.path.abspath(epub))
        
def main(epub):
    list_o_files = get_oebps_files(epub)  
    notes_filename = get_notes_filename(list_o_files)
    master_dict = create_page_anchor_dict(list_o_files)    
    rewritten_doms, new_notes_dom = write_new_notes_section(fix_hrefs_in_notes_section(list_o_files, notes_filename, master_dict), notes_filename)
    create_epub_with_new_notes(epub, new_notes_dom, rewritten_doms)

if __name__ == '__main__':
    logging.basicConfig(filename=correct_folder + '/log_filename.txt', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
    main(sys.argv[1])