from epub_file import EpubFile

# class Epub
#   methods:
#     add_file()       -- accepts single EpubFile
#     get_files()      -- returns list of EpubFiles
#     get_opf()        -- returns EpubFile for opf or None
#     get_ncx()        -- returns EpubFile for ncx or None
#     get_css_files()   -- returns list of EpubFiles (*.css)
#     get_xhtml_files() -- returns list of EpubFiles (*.htm, 
#                          *.html, *.xhtml)
#

class Epub:
	def __init__(self):
		self.files = []

	def add_file(self, epubFile):
		self.files.append(epubFile)

	def get_files(self):
		self.order_files()
		return self.files

	def order_files(self):
		for f in [x for x in self.files]:
			if f.get_path() == 'mimetype':
				self.files.remove(f)
				self.files.insert(0, f)

	def get_opf(self):
		for epubFile in self.files:
			path = epubFile.get_path()
			if path.endswith('.opf'):
				return epubFile
		return None

	def get_ncx(self):
		for epubFile in self.files:
			path = epubFile.get_path()
			if path.endswith('.ncx'):
				return epubFile
		return None

	def get_xhtml_files(self):
		xhtmlFiles = []
		for epubFile in self.files:
			path = epubFile.get_path()
			if path.endswith('.htm') or path.endswith('.html') or path.endswith('.xhtml'):
				xhtmlFiles.append(epubFile)
		return xhtmlFiles

	def get_css_files(self):
		cssFiles = []
		for epubFile in self.files:
			path = epubFile.get_path()
			if path.endswith('.css'):
				cssFiles.append(epubFile)
		return cssFiles

	# TODO: check for more metadata fields
	def get_metadata(self):
		meta = {
				'title': '',
				'isbn': '',
				'author': ''
				}
			
		try:
			# get isbn, title, and author from OPF
			dom = self.get_opf().get_dom()

			# <dc:title>Let The Great World Spin: A Novel</dc:title>

			dcTitleElements = dom.getElementsByTagName('dc:title')
			if len(dcTitleElements) != 0:
				meta['title'] = dcTitleElements[0].firstChild.nodeValue

			# <dc:identifier id="PrimaryID" 
			# opf:scheme="ISBN">978-1-58836-873-7</dc:identifier>

			dcIdentifierElements = dom.getElementsByTagName('dc:identifier')
			if len(dcIdentifierElements) != 0:
				meta['isbn'] = dcIdentifierElements[0].firstChild.nodeValue

			# <dc:creator opf:role="aut" 
			#     opf:file-as="McCann, Colum">Colum McCann</dc:creator>
			
			dcCreatorElements = dom.getElementsByTagName('dc:creator')
			if len(dcCreatorElements) != 0:
				meta['author'] = dcCreatorElements[0].firstChild.nodeValue

			# return dictionary
			return meta

		except Exception as ex:
			raise Exception('Exception occurred while getting ' \
					'metadata from OPF: ' + str(ex))
		
