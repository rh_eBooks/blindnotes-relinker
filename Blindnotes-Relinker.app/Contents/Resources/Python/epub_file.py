import xml.dom.minidom as minidom
import tempfile
import re
import os

# class EpubFile
#     methods:
#       set_data()     -- if you manipulate data, you'll need to 
#                         update the EpubFile using this method
#       get_data()     -- get "raw" data. Useful for dealing with 
#                         files on purely textual level
#       set_dom()      -- you shouldn't need to use this. the object 
#                         you get from get_dom() is live
#       get_dom()      -- get minidom Document object
#       get_path()     -- get path relative to root of EPUB package


# all epubfiles initially store in temp file
# if data requested, data stored in memory
# if dom requested, dom stored in memory

class EpubFile:
	def __init__(self, packagepath, data):
		tmpFileDescriptor, self._temp_file_path = tempfile.mkstemp()
		f = os.fdopen(tmpFileDescriptor,'w')
		f.write(data)
		f.close()
		self._data = None
		self._dom = None
		self.packagepath = packagepath
	# --------- get/set data ------------
	def set_data(self, data):	
		self._data = data
		self._remove_temp_file()
		self._dom = None
	def get_data(self):
		if self._dom:
			#get cleaned up version of xml
			return self._dom_to_escaped_string(self._dom) 
		elif self._data:
			return self._data
		else:
			# if still temp file, read into memory
			f = open(self._temp_file_path,'r')
			self._data = f.read()
			f.close()
			self._remove_temp_file()
			return self._data
	# ----------- get/set xml -------------	
	def set_dom(self, dom):
		self._dom = dom
		self._data = None
		self._remove_temp_file()
	def get_dom(self):
		if self._dom:
			return self._dom
		else:
			self._dom = minidom.parseString(self.get_data())
			self._data = None
			return self._dom
	#
	# Python's XML to text function does not provide
	# much customization. As such, need to do some
	# annoying text manipulation to get results we want.
	#
	# Using "pretty" output is very risky when dealing with
	# HTML, as it can mess up carefully calibrated whitespace.
	#
	def _dom_to_escaped_string(self, dom):
		txt = self._dom_to_string(dom)
		# add newline after xml declaration if necessary
		p1 = re.compile(r'(<\?xml[^?]*?\?>)(<)')
		txt = p1.sub(r'\1\n\2', txt) 
		# add newline after doctype if necessary
		p2 = re.compile(r'(<!DOCTYPE[^>]*?>)(<)')
		txt = p2.sub(r'\1\n\2', txt) 
		#
		# Not escaping in OPF to adhere to RH spec
		#
		if not self.get_path().lower().endswith('.opf'):
			txt = self.escape_unicode(txt) # using the Unicode escape function decreases speed dramatically
		return txt

	def _dom_to_string(self, dom):
		if self._is_html():
			return dom.toxml('utf-8')
		else:
			return dom.toxml('utf-8')

	def _is_html(self):
		path = self.get_path().lower()
		if path.endswith('.htm'):
			return True
		if path.endswith('.html'):
			return True
		if path.endswith('.xhtml'):
			return True
		return False

	#
	# TODO: Need more efficient way to implement Unicode escaping.
	# Going character by character is so inefficient. What's most
	# frustrating is that this step is not technically necessary. It
	# does help humans to see the entity references, though, and
	# it keeps the characters that are in the actual file all within
	# the ASCII range, which could be beneficial.
	#
	def escape_unicode(self, txt):
		txtEntities = ''
		for unicodeCharacter in txt.decode('utf-8'):
			c = unicodeCharacter
			ordinal = ord(c)
			if ordinal > 126:
				hexed = '&#x' + hex(ordinal)[2:] + ';'
				txtEntities = txtEntities + hexed
			else:
				txtEntities = txtEntities + c
		return txtEntities
	# ---------- meta --------------
	def set_path(self, path):
		self.packagepath = path
	def get_path(self):
		return self.packagepath
	def _remove_temp_file(self):
		if self._temp_file_path:
			os.remove(self._temp_file_path) # remove temp file
			self._temp_file_path = None
