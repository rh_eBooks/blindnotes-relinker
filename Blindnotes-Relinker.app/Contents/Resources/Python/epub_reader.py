import zipfile
import os
import tempfile
import time
import logging

logger = logging.getLogger(__name__)

from epub import Epub
from epub_file import EpubFile

class EpubReader:
	@classmethod
	def read_file(cls, path_to_epub):
		if os.path.isdir(path_to_epub):
			return cls.get_from_folder(path_to_epub)
		return cls.get_from_file(path_to_epub)

	@classmethod
	def get_from_file(cls, path_to_epub):
		zip_in = zipfile.ZipFile(path_to_epub, 'r')
		epub = Epub()
		for zip_info in zip_in.infolist():
			name = os.path.basename(zip_info.filename)	
			if cls.is_unwanted_file(zip_info.filename):
				logger.debug('ignore ' + name)
			else:
				#logger.debug('add ' + name)
				data = zip_in.read(zip_info)
				epub.add_file(EpubFile(zip_info.filename,data))
		zip_in.close()
		return epub

	@classmethod
	def get_from_folder(cls, path_to_epub):
		epub = Epub()
		for root, dirs, files in os.walk(path_to_epub):
			for file in files:
				abspath = os.path.join(root, file)
				packagepath = os.path.relpath(abspath, path_to_epub)
				if cls.is_unwanted_file(packagepath):
					logger.debug('ignore ' + os.path.basename(packagepath))
				else:
					logger.debug('add ' + os.path.basename(packagepath))
					f = open(abspath, 'r')
					data = f.read()
					epub.add_file(EpubFile(packagepath, data))
					f.close()
		return epub

	@classmethod
	def is_unwanted_file(cls, path):
		shouldRemove = False
		if path.lower().endswith('itunesmetadata.plist'):
			shouldRemove = True
		elif path.lower().endswith('.frk'):
			shouldRemove = True
		elif path.lower().endswith('thumbs.db'):
			shouldRemove = True
		elif os.path.basename(path).lower().startswith('.'): 
			shouldRemove = True
		return shouldRemove
