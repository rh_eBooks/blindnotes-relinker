import zipfile
import os
import tempfile
import time
import logging

logger = logging.getLogger(__name__)

class EpubWriter:
	@classmethod
	def write_file(cls, epub, output_path):
		if output_path.endswith('.epub') or output_path.endswith('.zip'):
			cls.write_to_file(epub, output_path)
		else:
			cls.write_to_folder(epub, output_path)

	@classmethod
	def write_folder(cls, epub, output_path):
		cls.write_to_folder(epub, output_path)

	@classmethod
	def write_to_file(cls, epub, output_path):
		logger.debug('writing epub to temp file')
		tmpFileDescriptor, tmpPath = tempfile.mkstemp()
		fileOut = os.fdopen(tmpFileDescriptor,'w')
		zipOut = zipfile.ZipFile(fileOut, 'w')
		epubFiles = epub.get_files()
		now = time.localtime(time.time())[:6] # timestamp for zipping
		logger.debug('timestamp: ' + str(now))
		for epubFile in epubFiles:
			entryPath = epubFile.get_path()
			logger.debug('writing entry: ' + entryPath)
			data = epubFile.get_data()
			# make new ZipInfo object
			info = zipfile.ZipInfo(entryPath)
			info.date_time = now
			if entryPath != 'mimetype':
				info.compress_type = zipfile.ZIP_DEFLATED
			zipOut.writestr(info, data )
		zipOut.close()
		fileOut.close()
		# overwrite the original epub
		# and remove tmp file
		logger.debug('moving written epub to ' + output_path)
		os.rename(tmpPath, output_path) 

	@classmethod
	def write_to_folder(cls, epub, output_path):
		logger.debug('writing epub to temp file')
		epubFiles = epub.get_files()
		for epubFile in epubFiles:
			entryPath = epubFile.get_path()
			fullpath = os.path.join(output_path, entryPath)
			data = epubFile.get_data()
			cls.realize_folderpath(os.path.dirname(fullpath))
			f = open(fullpath, 'w')
			f.write(data)
			f.close()

	@classmethod
	def realize_folderpath(cls, folderpath):
		if not os.path.exists(folderpath):
			if not os.path.exists(os.path.dirname(folderpath)):
				realize_folder(os.path.dirname(folderpath))
			os.mkdir(folderpath)
	
	@classmethod
	def realize_path(cls, path):
		print 'realizing %s' % (path)
		for root, dirs, files in os.walk(path):
			for folder in dirs:
				print 'checking %s' % (folder)
				if not os.path.exists(folder):
					print '  making %s' % (folder)
					os.mkdir(folder)

